import { MongoClient } from 'mongodb';
import { logger } from './log.js'
import { readFileSync } from 'node:fs';

// Keep track of DB connection
let db;

// Connect to the DB
export async function connectToDB() {
  // First try to get DB connection string from MONGODB_URL env var
  let uri = process.env.MONGODB_URL;

  // If uri emtpy try to get it from /app/db/mongo_url file
  if(!uri){
    logger.info('MONGODB_URL not provided in as an environment variable');
    try {
      uri = readFileSync('/app/db/mongo_url', 'utf8');
    } catch(error){
      logger.error(error);
    }
  }

  // If uri still empty consider local mongo
  if(! uri){
    logger.info('db connection string is not provided in local filesystem neither => local mongo will be used');
    uri = "mongodb://mongo:27017";
  }

  // Connection string provided
  logger.debug(`connection string is ${uri}`);

  // Make sure connection string does not contains funky char at the end
  uri = uri.replace(/(\r\n|\n|\r)/gm, "");

  // Connect to DB
  try {
      const mongoClient = new MongoClient(uri);
      await mongoClient.connect();
      logger.info('connected to MongoDB');
      db = mongoClient.db('message');
  } catch (error) {
      logger.error('connection to MongoDB failed!', error);
      process.exit();
  }
}

// Create a message
export async function createMessage(message) {
  logger.debug('db-createMessage');
  logger.debug(message);
  if (!db) {
    await connectToDB()()
  }

  const collection = db.collection('message');
  await collection.insertOne(message); 
}

// List all messages
export async function getMessages() {
  logger.debug('db-getMessages');
  if (!db) {
    await connectToDB()()
  }

  const collection = db.collection('message');
  const messages = collection.find({}).toArray();
  return messages;
}
