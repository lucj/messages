import express from 'express';
import { logger } from './log.js'
import { createMessage, getMessages } from './db.js'
  
const app = express();

// Parse application/json
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Handle message creation
app.post('/messages', function(req, res) {
    logger.debug('create message request');

    // Make sure the request contains a body
    if(! req.body){
        return res.status(400).json({error: 'no JSON payload provided'});
    }

    logger.debug(req.body);

    // Build message object to persist in DB
    let message = {};

    // Make sure 'msg' property is provided
    if(!req.body['msg']){
        return res.status(400).json({error: 'msg must be provided'});
    } else {
        // Make sure msg is a 100 chars max length string
        var msg = req.body['msg'];
        if(!msg.match(/^.{0,50}$/)){
            return res.status(400).json({error: 'msg must be a string with 100 chars max'});
        } else {
            message.msg = msg;
        }
    }

    // Timestamp message
    message.created_at = new Date().toISOString();

    // Persist message in database
    try{
        createMessage(message);

        // Remove database identifier
        delete message._id;

        return res.status(200).json(message);
    } catch(error){
        logger.error(error);
        return res.status(500).json({error: 'cannot persist message to database'});
    }
})

// List all messages
app.get('/messages', async function(req, res) {
    logger.debug('get messages request');
    try{
        const messages = await getMessages();
        console.log(messages);
        
        return res.status(200).json(messages);
    } catch(error){
        logger.error(error);
        return res.status(500).json({error: 'cannot get messages from database'});
    }
})

const PORT = 3000;
  
export async function runWebServer() {
  app.listen(3000,() => {
    logger.info(`Running on PORT ${PORT}`);
  })
}
