docker buildx create --name mybuilder

docker buildx build . \
  --target production \
  --platform linux/amd64,linux/arm64,linux/arm/v7 \
  -t lucj/messages:1.2 \
  --push
