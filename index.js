import { connectToDB } from './lib/db.js'
import { runWebServer } from './lib/www.js';

const main = async () => {
  await connectToDB();
  await runWebServer();
};

main();
