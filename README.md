## Purposes

Used mainly for demo, this simple project exposes an HTTP API that enable to:
- create a message
- list existing messages

Messages are persisted in a MongoDB database whose connection string needs to be provided at startup, either:
- in the MONGODB_URL environment variable
- as a file mounted in /run/secrets/mongodb_url

## Testing the thing

First run the application with Docker Compose:

```
docker compose up -d
```

Next, send a message

```
curl -XPOST -H 'Content-Type: application/json' -d '{"msg":"hello guys"}' http://localhost:3000/messages
```

Then list the existing messages:

```
curl -q http://localhost:3000/messages | jq .
```